ThinkNN is a C++ implementation of a Multilayer Perceptron.  Utilizes the hyperbolic tangent as the transfer function.  Derivative of the hyperbolic tangent is estimated for efficiency and simplicity.  Implements MLP feed-forward and standard backpropagation for training.

This is a Netbeans Project.

