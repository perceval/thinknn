/* 
 * File:   NeuralNetwork.cpp
 * Author: perceval (kragbrowder@gmail.com)
 * 
 * Created on October 22, 2013, 9:25 PM
 */

#include "NeuralNetwork.h"
#include <iostream>
#include <cmath>

NeuralNetwork::NeuralNetwork(const vector<int>& topology, const double learnRate, const double stepSize)
{
    this->numLayers = topology.size();
    for(int iLayer = 0; iLayer < numLayers; ++iLayer)
    {
        this->layers.push_back(Layer());
        cout << "------- Created a layer... " <<endl;
        //find neuron's number of outputs for each layer
        // if neuron is output neuron, number of outputs = 0, for everything else
        // number of outputs is equal to the number of neurons in the next layer over
        int numOutput = iLayer == topology.size() - 1 ? 0 : topology[iLayer + 1];
        
        // <= topology[iLayer] includes bias neuron
        for(int iNeuron = 0 ; iNeuron <= topology[iLayer]; ++iNeuron)
        {
            this->layers.back().push_back(Neuron(numOutput, iNeuron, learnRate, stepSize));
            cout << "Created a neuron... " <<endl;
        }
        
        //Last neuron created above is the bias neuron so set it's value to 1
        this->layers.back().back().setOutputValue(1.0);
        
    }
    
}

NeuralNetwork::NeuralNetwork(const NeuralNetwork& orig) {
}

NeuralNetwork::~NeuralNetwork() {
}

void NeuralNetwork::feedForward(const vector<double>& inputs)
{
    //check # of input vals is the same of # of input neurons
    assert(inputs.size() == layers[0].size()-1);
    
    //set input values to input neurons
    for(int i = 0; i < inputs.size(); i++)
    {
        layers[0][i].setOutputValue(inputs[i]);
    }
    
    //feed forward time
    for(int iLayer = 1; iLayer < layers.size(); ++iLayer)
    {
        Layer& previousLayer = layers[iLayer - 1];
        for(int iNode = 0; iNode < layers[iLayer].size() - 1; ++iNode)
        {
            layers[iLayer][iNode].feedForward(previousLayer);
        }
    }
    
}

void NeuralNetwork::backPropagation(const vector<double>& targets)
{
    // Calc overall error of the network (Root-Mean-Square)
    // RMS = sqrt(1/n * Sum i to n(Targeti - Actuali)^2 )
    Layer &outputLayer = this->layers.back();
    this->error = 0.0;
    
    for(int i = 0; i < outputLayer.size() - 1; ++i)
    {
        double delta = targets[i] - outputLayer[i].getOutputValue();
        this->error += delta * delta;
    }
    
    this->error /= outputLayer.size() - 1;
    this->error = sqrt(this->error);
    
    // Find recent average error for display purpose during training.
    this->averageRecentError = 
            (this->averageRecentError * this->averageRecentSmoothFactor + this->error) 
            / (this->averageRecentSmoothFactor + 1.0);
    
            
    // Calc output layer gradients (hidden)
    for(int i = 0; i < outputLayer.size() - 1; ++i)
    {
        outputLayer[i].calcOutputGradients(targets[i]);
    }
    // Calc gradients n hidden layers
    for(int l = layers.size() - 2; l > 0; --l)
    {
        Layer &currentHiddenLayer = layers[l];
        Layer &nextLayer = layers[l + 1];
        
        for(int i = 0; i < currentHiddenLayer.size(); ++i)
        {
            currentHiddenLayer[i].calcHiddenGradients(nextLayer);
        }
    }
    // Calc weights from output to first hidden layer
    for(int l = layers.size() -1; l > 0; --l)
    {
        Layer &layer = layers[l];
        Layer &previouslayer = layers[l - 1];
        
        //Loop through neurons in curent layer
        for(int i = 0; i < layer.size() -1; ++i)
        {
            layer[i].updateInputWeights(previouslayer);
        }
    
    }
    
    
}

void NeuralNetwork::getResults(vector<double> result) const
{
    result.clear();
    
    for(int i = 0; i < this->layers.back().size() - 1; ++i)
    {
        result.push_back(this->layers.back()[i].getOutputValue());
    }
}