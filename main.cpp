/* 
 * File:   main.cpp
 * Author: perceval
 *
 * Created on October 22, 2013, 9:21 PM
 */

#include <cstdlib>
#include <vector>
#include "NeuralNetwork.h"
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    vector<int> topology;
    topology.push_back(3);
    topology.push_back(4);
    topology.push_back(4);
    topology.push_back(1);
    
    //learning rate of the network [0.0 to 1.0] 
    // (0 is slow but 1 is more reckless)
    double eta = 0.15;
    //step size or momentum of the learning algorithm [0.0 to n]
    double alpha = 0.5;
    
    NeuralNetwork nn(topology, eta, alpha);
    
    /*
    vector<double> inputVector;
    nn.feedForward(inputVector);
    
    vector<double> targets;
    nn.backPropagation(targets);
    
    vector<double> results;
    nn.getResults(results);
    */
    
    return 0;
}

