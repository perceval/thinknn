/* 
 * File:   NeuralNetwork.h
 * Author: perceval
 *
 * Created on October 22, 2013, 9:25 PM
 */
#ifndef NEURALNETWORK_H
#define	NEURALNETWORK_H
#include <vector>
#include <cassert>
#include "Neuron.h"

using namespace std;

class NeuralNetwork {
    
typedef vector<Neuron> Layer;
    
public:
    NeuralNetwork(const vector<int>& topology, const double learnRate, const double stepSize);
    NeuralNetwork(const NeuralNetwork& orig);
    virtual ~NeuralNetwork();
    
    void feedForward(const vector<double>& inputs);
    void backPropagation(const vector<double>& targets);
    void getResults(vector<double> result) const;
    
    
private:
    vector<Layer> layers;
    int numLayers;
    double error;
    double averageRecentError;
    double averageRecentSmoothFactor;

};

#endif	/* NEURALNETWORK_H */

