/* 
 * File:   Neuron.cpp
 * Author: perceval
 * 
 * Created on October 22, 2013, 10:12 PM
 */
#include <cmath>
#include "Neuron.h"

Neuron::Neuron(int numOutputs, int neuronIndex, const double learnRate, const double stepSize) 
{
    for(int i = 0; i < numOutputs; ++i)
    {
        outputWeights.push_back(Connection());
        outputWeights.back().weight = randWeight();
    }
    
    this->neuronIndex = neuronIndex;
    this->learnRate = learnRate;
    this->stepSize = stepSize;
}

Neuron::Neuron(const Neuron& orig) 
{
}

Neuron::~Neuron() 
{
}

void Neuron::setOutputValue(double val)
{
    this->outputValue = val;
}
double Neuron::getOutputValue() const
{
    this->outputValue;
}

double Neuron::randWeight(void)
{
    return (rand() / double(RAND_MAX) );
}

// output = f(sum(IiWi))
void Neuron::feedForward(const Layer& previousLayer)
{
    double sum = 0.0;
    for(int i = 0; i < previousLayer.size(); ++i)
    {
        Neuron tNeuron = previousLayer[i];
        sum += tNeuron.getOutputValue() * tNeuron.outputWeights[neuronIndex].weight;
    }
    
    this->outputValue = transferFunction(sum);
}

double Neuron::transferFunction(double val)
{
    return tanh(val);
}

double Neuron::transferFunctionDerivative(double val)
{
    //estimate for calculating hyperbolic tangent derivative
    //this must change if transfer function is not tanh()
    
    return 1 - val * val;
}

void Neuron::calcOutputGradients(double targetValue)
{
    double errDelta = targetValue - this->outputValue;
    this->gradient = errDelta * this->transferFunction(this->outputValue);
}

void Neuron::calcHiddenGradients(const Layer &nextLayer)
{
    //Error delta is sum of derivative of weights in nextLayer
    double errDelta = 0.0;

    //Sum connection weight of neuron * neuron in next layer's gradient
    for(int i = 0; i < nextLayer.size() - 1; ++i)
    {
        errDelta += this->outputWeights[i].weight * nextLayer[i].gradient;
    }

    this->gradient = errDelta * this->transferFunctionDerivative(this->outputValue);
}

void Neuron::updateInputWeights(Layer& previousLayer)
{
    //Update the Connection container's weights in neurons on previous layer
    
    for(int i = 0; i < previousLayer.size(); ++i)
    {
        Neuron &neuron = previousLayer[i];
        double oldDeltaWeight = neuron.outputWeights[this->neuronIndex].deltaWeight;
        
        double newDeltaWeight = this->learnRate *
                neuron.getOutputValue() *
                this->gradient *
                this->stepSize *
                oldDeltaWeight;
        
        neuron.outputWeights[this->neuronIndex].deltaWeight = newDeltaWeight;
        neuron.outputWeights[this->neuronIndex].weight += newDeltaWeight;
    }
    
}