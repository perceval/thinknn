/* 
 * File:   Neuron.h
 * Author: perceval
 *
 * Created on October 22, 2013, 10:12 PM
 */

#ifndef NEURON_H
#define	NEURON_H
#include <vector>
#include <cstdlib>

using namespace std;

struct Connection
{
    double weight;
    double deltaWeight;
};

class Neuron {
    
typedef vector<Neuron> Layer;

public:
    
    Neuron(int numOutputs, int neuronIndex, const double learnRate, const double stepSize);
    Neuron(const Neuron& orig);
    virtual ~Neuron();
    
    void setOutputValue (double outputValue);
    double getOutputValue (void) const;
    
    void feedForward(const Layer& previousLayer);
    void calcOutputGradients(double targetValue);
    void calcHiddenGradients(const Layer& nextLayer);
    void updateInputWeights(Layer &previousLayer);
    
private:
    double randWeight(void);
    double transferFunction(double);
    double transferFunctionDerivative(double);
    vector<Connection> outputWeights;
    double outputValue;
    int neuronIndex;
    double gradient;
    double learnRate;
    double stepSize;

};

#endif	/* NEURON_H */

